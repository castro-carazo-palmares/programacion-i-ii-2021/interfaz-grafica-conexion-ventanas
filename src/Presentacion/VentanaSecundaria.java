package Presentacion;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VentanaSecundaria {
    private JPanel panel;
    private JButton btnRegresar;

    public VentanaSecundaria() {
        JFrame frame = new JFrame("Ventana Secundaria");
        frame.setLayout(null);
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.setSize(300, 300);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        panel.setLayout(null);

        btnRegresar.setBounds(75, 75, 150, 50);

        btnRegresar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                VentanaPrincipal.ventanaPrincipal.getFrame().setVisible(true);
            }
        });
    }
}
