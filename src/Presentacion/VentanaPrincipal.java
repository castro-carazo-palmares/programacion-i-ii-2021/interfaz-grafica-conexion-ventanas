package Presentacion;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VentanaPrincipal {
    private JPanel panel;
    private JButton btnVentana;
    static VentanaPrincipal ventanaPrincipal;
    private JFrame frame;

    public VentanaPrincipal() {
        frame = new JFrame("Ventana Principal");
        frame.setLayout(null);
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 300);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        panel.setLayout(null);

        btnVentana.setBounds(75, 75, 150, 50);

        btnVentana.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.setVisible(false);
                new VentanaSecundaria();
            }
        });
    }

    public JFrame getFrame() {
        return frame;
    }

    public static void main(String[] args) {
        ventanaPrincipal = new VentanaPrincipal();
    }
}
